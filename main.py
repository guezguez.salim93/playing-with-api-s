import requests
import pycountry
import mysql.connector


def get_countries(countries_dic):
    countries_list = countries_dic['country']
    result = []
    for country in countries_list:
        result.append(country['country_id'])
    return result


def get_gender(name):
    query = {'name': name}
    response_gender = requests.get("https://api.genderize.io", params=query)
    return response_gender.json()['gender'] + " with " + str(response_gender.json()['probability']) + ' Probability'


def get_age(name):
    query = {'name': name}
    response_age = requests.get("https://api.agify.io", params=query)
    return response_age.json()['age']


def get_nationality(name):
    query = {'name': name}
    response_nationality = requests.get("https://api.nationalize.io", params=query)
    return [pycountry.countries.get(alpha_2=name).name for name in get_countries(response_nationality.json())]


def save_to_database(name, gender, age, countries):
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="gzgz",
        database="name_information"
    )
    mycursor = mydb.cursor()
    sql_command = "INSERT INTO informations (name, gender, age, countries) VALUES (%s,%s,%s,%s) "
    information_tupel = (name, gender, age, countries)
    mycursor.execute(sql_command, information_tupel)
    mydb.commit()


def main():
    name = input('choose a name :')
    age = get_age(name)
    gender = get_gender(name)
    countries = get_nationality(name)
    print(f'{gender} age = {age} possible countries = {countries}')
    response = input('do you want to store the output in your database: y/n')
    if response == 'y':
        save_to_database(name, gender, age, str(countries))


if __name__ == "__main__":
    main()
